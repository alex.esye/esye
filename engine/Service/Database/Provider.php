<?php

/**
 * Database Service Provider.
 */

namespace Esye\Service\Database;

use Esye\Service\AbstractProvider;
use Esye\Core\Database\Connection;

class Provider extends AbstractProvider
{
    /* Service name. */
    public $serviceName = 'database';

    /**
     * Init provider.
     */
    public function initialize(){

        // New Database.
        $database = new Connection();

        /* Set configs in DI. */
        $this->di->set($this->serviceName, $database);

    }
}
