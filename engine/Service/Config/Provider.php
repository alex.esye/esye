<?php

/**
 * Config Service Provider.
 */

namespace Esye\Service\Config;

use Esye\Service\AbstractProvider;
use Esye\Core\Config\Config;

class Provider extends AbstractProvider
{
    /* Service name. */
    public $serviceName = 'config';

    /**
     * Init provider.
     */
    public function initialize(){

        /* Get configs. */
        $config['main']     = Config::group('main');
        $config['database'] = Config::group('database');

        /* Set configs in DI. */
        $this->di->set($this->serviceName, $config);

    }
}
