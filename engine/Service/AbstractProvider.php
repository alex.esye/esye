<?php

/**
 * Service abstract Provider.
 */

namespace Esye\Service;

abstract class AbstractProvider
{
    protected $di;

    /**
     * AbstractProvider constructor.
     */
    public function __construct($di)
    {
        $this->di = $di;
    }

    /**
     * Abstract init service function.
     */
    abstract function initialize();
}
