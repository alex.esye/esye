<?php

/**
 * Request Service Provider.
 */

namespace Esye\Service\Request;

use Esye\Service\AbstractProvider;
use Esye\Core\Request\Request;

class Provider extends AbstractProvider
{
    /* Service name. */
    public $serviceName = 'request';

    /**
     * Init provider.
     */
    public function initialize(){

        /* New Request. */
        $request = new Request();

        /* Set Request in DI. */
        $this->di->set($this->serviceName, $request);
    }
}
