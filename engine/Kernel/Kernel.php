<?php

namespace Esye\Kernel;

use Esye\Kernel\DependencyInjection\Container;

class Kernel
{
    // Dependency Injection.
    protected $di;

    // Booted status.
    protected $booted = false;

    public function __construct()
    {
        // Check kernel booted.
        if ($this->booted == false) {
            // Init kernel.
            $this->init();
        }

        echo 'Kernel was booted.';

    }

    /**
     * Init kernel.
     */
    private function init()
    {
        // New DI Container.
        $this->di = new Container();

        // Initialize services.
        $this->services();

        // Change booted status.
        $this->booted = true;
    }

    /**
     * Initialize services.
     */
    private function services()
    {
        // Get services list.
        $services = require_once ROOT_DIR . '/engine/Config/Service.php';

        // Initialize services.
        foreach ($services as $service) {
            $service = new $service($this->di);
            $service->initialize();
        }
    }
}
