<?php

namespace Esye\Kernel\DependencyInjection;

class Container
{
    protected $container = [];

    public function set($key, $value)
    {
        $this->container[$key] = $value;
    }

    public function get($key)
    {
        if($this->isset($key)) return $this->container[$key];

        return [];
    }

    public function isset($key)
    {
        if(array_key_exists($key, $this->container)) return true;

        return false;
    }
}
