<?php

// Composer autoload.
require_once ROOT_DIR . '/vendor/autoload.php';

use Esye\Kernel\Kernel;

// Boot new Kernel.
new Kernel();
