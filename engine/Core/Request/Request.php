<?php

/**
 * Request.
 */

namespace Esye\Core\Request;

class Request
{
    public $get = [];

    public $post = [];

    public $request = [];

    public $cookie = [];

    public $files = [];

    public $server = [];

    /**
     * Request constructor.
     */
    public function __construct()
    {
        $this->get     = $_GET;
        $this->post    = $_POST;
        $this->request = $_REQUEST;
        $this->cookie  = $_COOKIE;
        $this->files   = $_FILES;
        $this->server  = $_SERVER;
    }

    public static function getRequestUri()
    {
        $url = $_SERVER['REQUEST_URI'];

        $url = preg_replace('|([/]+)|s', '/', $url);
        if (substr($url, -1) !== '/') $url .= '/';
        
        return $url;
    }

    public static function getRequestMethod()
    {
        return $_SERVER['REQUEST_METHOD'];
    }
}
