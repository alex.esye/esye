<?php

return [
    Esye\Service\Request\Provider::class,
    Esye\Service\Config\Provider::class,
    Esye\Service\Database\Provider::class
];
