<?php

/**
 * Example main settings file.
 */

return [
    'website_url' => 'http://test1.ru',
    'website_https' => 'false',

    'timezone' => 'Europe/Moscow',
    
    'webmaster_email' => 'webmaster@test1.ru'
];
