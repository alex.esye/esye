<?php

/**
 * Example database settings file.
 */

return [
    'host'     => 'localhost',
    'username' => 'root',
    'password' => '',
    'database' => 'esye_database',
    'charset'  => 'utf8'
];
