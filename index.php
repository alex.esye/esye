<?php

/**
 * Alexander Lysyakov. 2024 (C) Все права зищащены.
 * https://gitlab.com/alex.esye/esye.git
 */

// Root directory constant.
define('ROOT_DIR', __DIR__);

// Require bootstrap file.
require_once ROOT_DIR . '/engine/bootstrap.php';
